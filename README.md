# README #
I used [liberty](https://openliberty.io/downloads/#runtime_releases) (20.0.0.12) to run the ear and I used [DB Derby](https://db.apache.org/derby/releases/release-10_15_2_0.cgi) (10.15.2.0) as a database engine.

The [server.xml](https://bitbucket.org/brimirnl/example-jpa/src/master/server.xml) for liberty can be found in this repository.
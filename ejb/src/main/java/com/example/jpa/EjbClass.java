package com.example.jpa;

import com.example.jpa.entity.TestEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class EjbClass {

    @PersistenceContext
    private EntityManager entityManager;

    public List<TestEntity> test(){
        return this.entityManager.createNamedQuery("test.query", TestEntity.class)
                .setParameter("field1","test")
                .getResultList();
    }
}

package com.example.jpa.entity;

import javax.persistence.*;

@Entity
@NamedQueries(
        @NamedQuery(
                name = "test.query",
                query = "select t from TestEntity t where t.field1 = :field1 AND t.field1 = :field1 AND t.field1 = :field1 AND t.field1 = :field1"
        )
)
public class TestEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String field1;
    @Column
    private String field2;
    @Column
    private String field3;
    @Column
    private String field4;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getField4() {
        return field4;
    }

    public void setField4(String field4) {
        this.field4 = field4;
    }
}

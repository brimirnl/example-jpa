package com.example.jpa;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
public class Controller {

    @Inject
    private EjbClass ejbClass;

    @Path("")
    @GET
    public Response test() {
        return Response.ok()
                .entity(ejbClass.test())
                .header("content-type", MediaType.APPLICATION_JSON)
                .build();
    }
}
